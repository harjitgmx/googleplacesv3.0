//
//  ViewController.swift
//  mapDemo
//
//  Created by Harjit Singh on 07/11/19.
//  Copyright © 2019 gmx. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import SDWebImage
public class MapViewController: UIViewController,GMSMapViewDelegate, CLLocationManagerDelegate{
    
    var placesClient: GMSPlacesClient!
    var gmsMapView: GMSMapView!
    
    var locationManager = CLLocationManager()
    var userCurrentLocation: CLLocationCoordinate2D?
    let tableView = UITableView.init(frame: CGRect.zero, style: .grouped)
    var timer = Timer()
    var tableViewFrame = CGRect()
    var tableviewInitialHeight = CGFloat()
    var nearbyLocationData :NearbyLocationDataModel?{
        didSet{
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    // var nearbyData : [GMSPlaceLikelihood]?
    let cellIdentifier = "placeCell"
    
    //    var updatedLocationLat = ""
    //    var updatedLocationLong = ""
    
    private lazy var mapBaseView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/1.7))
    
    // Add a pair of UILabels in Interface Builder, and connect the outlets to these variables.
    //    @IBOutlet var nameLabel: UILabel!
    //    @IBOutlet var addressLabel: UILabel!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        tableConfig()
        self.view.backgroundColor = .blue
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.startUpdatingLocation()
        placesClient = GMSPlacesClient.shared()
        // self.scrollViewDidScroll(tableView)
        userCurrentLocation = locationManager.location?.coordinate
        nearbyPlaces(latitude: userCurrentLocation?.latitude ?? 0.0, longitude: userCurrentLocation?.longitude ?? 0.0)
        
        //   getCurrentPlace()
        navigationConfiguration()
        loadMapView()
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        locationManager.startUpdatingLocation()
        self.view.addSubview(mapBaseView)
    }
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        self.tableViewFrame = self.tableView.frame
        self.tableviewInitialHeight = self.tableView.frame.height
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    // Add a UIButton in Interface Builder, and connect the action to this function.
    //    func getCurrentPlace() {
    //
    //        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
    //            if let error = error {
    //                print("Current Place error: \(error.localizedDescription)")
    //                return
    //            }
    //
    //            // self.nameLabel.text = "No current place"
    //            // self.addressLabel.text = ""
    //
    //            if let placeLikelihoodList = placeLikelihoodList {
    //                let place = placeLikelihoodList.likelihoods.first?.place
    //                if let place = place {
    //                    // self.nameLabel.text = place.name
    //                    // self.addressLabel.text = place.formattedAddress?.components(separatedBy: ", ")
    //                    // .joined(separator: "\n")
    //                }
    //            }
    //        })
    //    }
    
    func loadMapView() {
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 15)
        
        gmsMapView = GMSMapView.map(withFrame: mapBaseView.frame, camera: camera)
        gmsMapView.settings.myLocationButton = true
        gmsMapView.isMyLocationEnabled = true
        gmsMapView.delegate = self
        mapBaseView = gmsMapView
        self.gmsMapView?.animate(toLocation: locationManager.location?.coordinate ?? CLLocationCoordinate2D(latitude: 0, longitude: 0) )
        nearbyPlaces(latitude: locationManager.location?.coordinate.latitude ?? 0, longitude: locationManager.location?.coordinate.latitude ?? 0)
        let imageViewMarker = UIImageView()
        imageViewMarker.frame.size = CGSize(width: 36, height: 36)
        imageViewMarker.contentMode = .scaleAspectFit
        imageViewMarker.image = UIImage(named: "marker")
        imageViewMarker.center = mapBaseView.center
        mapBaseView.addSubview(imageViewMarker)
        appearance()
    }
    
    //    func nearbyPlaces(){
    //        // Specify the place data types to return.
    //        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.all.rawValue) )!
    ////        placesClient.
    //        placesClient?.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: fields, callback: {
    //            (placeLikelihoodList: Array<GMSPlaceLikelihood>?, error: Error?) in
    //            if let error = error {
    //                print("An error occurred: \(error.localizedDescription)")
    //                return
    //            }
    //
    //            if let placeLikelihoodList = placeLikelihoodList {
    //                for likelihood in placeLikelihoodList {
    //                    //let place = likelihood.place
    //                    print("NAME----[ \(String(describing: likelihood.place.name!)) ]")
    //                    print("ADDRESS-> \(likelihood.place.formattedAddress!)")
    //                    print(likelihood.place.types)
    //                    print("")
    //
    //                }
    //
    //                self.nearbyData = placeLikelihoodList
    //                self.tableView.reloadData()
    //
    //            }
    //        })
    //
    //    }
    func nearbyPlaces(latitude:Double,longitude:Double){
        let location = "\(latitude),\(longitude)"
        let soyouWebProductionKey = "AIzaSyDui-a2LUylvQDgfdBnHTCTS6vN8-ElWnE"
        let parameters = ["location":location,"radius":"1000","key":soyouWebProductionKey]
        let urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
        
        sendRequest(urlString, parameters: parameters) { data,responseObject, error in
            guard let responseObject = responseObject, error == nil else {
                print(error ?? "Unknown error")
                return
            }
            self.nearbyLocationData = try? JSONDecoder().decode(NearbyLocationDataModel.self, from: data!)
            
            print(responseObject)
        }
    }
    
    
    public func mapView(_ mapView: GMSMapView, willMove gesture: Bool){
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: (#selector(updateNerbyListOnMapDrag)), userInfo: nil, repeats: false)
    }
    
    @objc func updateNerbyListOnMapDrag(){
        let centreCordinate = gmsMapView.projection.coordinate(for: gmsMapView.center)
        nearbyPlaces(latitude: centreCordinate.latitude, longitude: centreCordinate.longitude)
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation = locations.last
        nearbyPlaces(latitude: currentLocation?.coordinate.latitude ?? 0.0, longitude: currentLocation?.coordinate.longitude ?? 0.0)
    }
    
    private func appearance(){
        let serachDisplayView = SearchDisplayController(x: 20, y: 30, height: 50, width:UIScreen.main.bounds.width - 40)
        serachDisplayView.addTarget(self, action: #selector(self.actionGoToAutoCompletion(sender:)), for: .touchUpInside)
        self.mapBaseView.addSubview(serachDisplayView)
        
        let selectThisLocationView = SelectCurrentLocationController(y: mapBaseView.frame.height)
        selectThisLocationView.addTarget(self, action: #selector(self.actionSelectThisLocation), for: .touchUpInside)
        self.view.addSubview(selectThisLocationView)
        
        let spaceForList = mapBaseView.frame.height + selectThisLocationView.frame.height
        tableView.frame = CGRect(x: 0, y: spaceForList, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - spaceForList)
        
        self.view.addSubview(tableView)
    }
    
    private func tableConfig(){
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        self.tableView.register(PlaceTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        self.tableView.backgroundColor = UIColor.init(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        
    }
    
    private func navigationConfiguration(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(actionLeftBarButtonItem))
        
        let rightBarButton = UIButton(type: .custom)
        rightBarButton.setImage(UIImage(named:"searchAbLight"), for: .normal)
        rightBarButton.addTarget(self, action: #selector(actionGoToAutoCompletion), for: UIControl.Event.touchUpInside)
        rightBarButton.imageView?.tintColor = .black
        let menuBarItem = UIBarButtonItem(customView: rightBarButton)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 20)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 20)
        currHeight?.isActive = true
        self.navigationItem.rightBarButtonItem = menuBarItem
        
        self.navigationController?.navigationBar.tintColor = .black
        let titleLabel = UILabel( )
        let navTitle = NSMutableAttributedString(string: "Select a location", attributes:[
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.medium)])
        titleLabel.attributedText = navTitle
        self.navigationItem.titleView = titleLabel
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    //MARK: actions
    
    @objc func actionGoToAutoCompletion(sender: UIButton){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.modalPresentationStyle = .fullScreen
        
     //   present(autocompleteController, animated: true, completion: nil)
                self.present(autocompleteController, animated: true, pushing: true) {
        
                }
        
    }
    
    @objc func actionSelectThisLocation(sender: UIButton){
        
    }
    
    @objc func actionLeftBarButtonItem(){
        
    }
    //    //MARK:- ScrollView Delegates
    //    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        view.bringSubviewToFront(tableView)
    //
    //        let y = UIScreen.main.bounds.height - tableviewInitialHeight - tableView.contentOffset.y
    //        let hight = tableviewInitialHeight + tableView.contentOffset.y
    //        self.tableView.frame = CGRect(x: 0, y: y, width: UIScreen.main.bounds.width, height: hight)
    //
    //        print(tableView.contentOffset.y)
    //
    //    }
    
}
extension MapViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table view data source & Delegates
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = nearbyLocationData?.results?.count{
            return count
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.frame.size.width - 15, height: 40))
        label.font = UIFont(name: "Helvetica", size: 12)
        label.text = "Or a choose nearby place"
        label.textColor = UIColor.init(red: 114/255, green: 114/255, blue: 114/255, alpha: 1)
        headerView.addSubview(label)
        return headerView
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PlaceTableViewCell
        cell.shortAddressLabel.text = nearbyLocationData?.results?[indexPath.row].name
        cell.fullAddressLabel.text = nearbyLocationData?.results?[indexPath.row].vicinity
        let iconImageUrl = URL(string: nearbyLocationData?.results?[indexPath.row].icon ?? "")
        cell.placeTypeImageView.sd_setImage(with: iconImageUrl, placeholderImage: UIImage(named: "location"))
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  50
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  50
    }
    
    //MARK:-API to get nearby location
    func sendRequest(_ url: String, parameters: [String: String], completion: @escaping (Data?,[String: Any]?, Error?) -> Void) {
        var components = URLComponents(string: url)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        let request = URLRequest(url: components.url!)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,                            // is there data
                let response = response as? HTTPURLResponse,  // is there HTTP response
                (200 ..< 300) ~= response.statusCode,         // is statusCode 2XX
                error == nil else {                           // was there no error, otherwise ...
                    completion(nil, nil, error)
                    return
            }
            
            let responseObject = (try? JSONSerialization.jsonObject(with: data)) as? [String: Any]
            completion(data, responseObject, nil)
        }
        task.resume()
    }
    
}
extension MapViewController:GMSAutocompleteViewControllerDelegate{
    
    public func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15)
        gmsMapView.animate(to: camera)
        dismiss(animated: true, completion: nil)
    }
    
    public func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    public func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    public func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    public func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}


extension UIViewController {
    
    open func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, pushing: Bool, completion: (() -> Void)? = nil) {
        
        if pushing {
            
            let transition = CATransition()
            transition.subtype = CATransitionSubtype.fromRight
            transition.duration = 0.3
            transition.type = CATransitionType.push
            viewControllerToPresent.modalPresentationStyle = .fullScreen
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            view.window?.backgroundColor = .white
            view.window?.layer.add(transition, forKey: kCATransition)
            
            self.present(viewControllerToPresent, animated: false, completion: completion)
            
        } else {
            self.present(viewControllerToPresent, animated: flag, completion: completion)
        }
        
    }
    
    
    
}
