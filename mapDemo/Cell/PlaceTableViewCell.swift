//
//  PlaceTableViewCell.swift
//  mapDemo
//
//  Created by Harjit Singh on 12/11/19.
//  Copyright © 2019 gmx. All rights reserved.
//
import GooglePlaces
import UIKit

class PlaceTableViewCell: UITableViewCell {
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override init(style:UITableViewCell.CellStyle,reuseIdentifier:String?){
        super.init(style:style, reuseIdentifier:reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = .clear
        placeTypeImageView = UIImageView(frame: CGRect(x: 15, y: self.frame.height/2 - 12, width: 24, height: 24))
        placeTypeImageView.contentMode = .scaleAspectFit
        addSubview(placeTypeImageView)
        let labelStack = UIStackView(arrangedSubviews: [shortAddressLabel, fullAddressLabel])
        labelStack.frame = CGRect(x: 50, y: 5, width: self.frame.width - 70, height: self.frame.height - 5)
        labelStack.axis = .vertical
        labelStack.alignment = .leading
        labelStack.spacing = 0
        addSubview(labelStack)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var placeTypeImageView = UIImageView()
    
    var shortAddressLabel : UILabel = {
            var label = UILabel()
            label.font = UIFont(name: "Helvetica", size: 14)
            label.text = "Select this location"
            label.textColor = .black
            return label
     }()
    
    var fullAddressLabel : UILabel = {
           var label = UILabel()
           label.numberOfLines = 0
           label.font = UIFont(name: "Helvetica", size: 10)
           label.text = "#591, 22nd Cross Rd, 15th Main Rd, 3rd Sector, HSR Layout, Bengaluru, 560102 "
           label.textColor = UIColor.init(red: 114/255, green: 114/255, blue: 114/255, alpha: 1)
           return label
    }()
    
     
}
