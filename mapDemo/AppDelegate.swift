//
//  AppDelegate.swift
//  mapDemo
//
//  Created by Harjit Singh on 07/11/19.
//  Copyright © 2019 gmx. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window : UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        //MARK: GoogleKeys
        GMSPlacesClient.provideAPIKey("AIzaSyB1lbVIOXZlbD2if3JBpliHXNG4ivRF3FM")
        GMSServices.provideAPIKey("AIzaSyB1lbVIOXZlbD2if3JBpliHXNG4ivRF3FM")
         
        
        let mainViewController = MapViewController()
        let mainNavigationController = UINavigationController(rootViewController: mainViewController)
        window?.rootViewController = mainNavigationController
        window?.makeKeyAndVisible()
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
}

