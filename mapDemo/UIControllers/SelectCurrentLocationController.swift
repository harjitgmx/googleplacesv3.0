//
//  SelectCurrentLocationController.swift
//  mapDemo
//
//  Created by Harjit Singh on 11/11/19.
//  Copyright © 2019 gmx. All rights reserved.
//

import UIKit

class SelectCurrentLocationController: UIControl {

    init(x:CGFloat = 0, y:CGFloat ) {
        super.init(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: 50))
        initializeSetup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initializeSetup(){
        self.backgroundColor = .white
        let imageViewLocation = UIImageView(frame: CGRect(x: 15, y: self.frame.height/2 - 12, width: 24, height: 24))
        imageViewLocation.image = UIImage(named: "location")
        self.addSubview(imageViewLocation)
        
        let selectLocationTextlabel = UILabel(frame: CGRect(x:  50 , y: 0, width: self.frame.width - 100, height: self.frame.height))
               selectLocationTextlabel.font = UIFont(name: "Helvetica", size: 14)
               selectLocationTextlabel.text = "Select this location"
               selectLocationTextlabel.textColor = .black
               self.addSubview(selectLocationTextlabel)
    }
    
}
