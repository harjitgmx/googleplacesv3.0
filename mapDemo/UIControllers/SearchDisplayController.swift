//
//  SearchDisplayController.swift
//  mapDemo
//
//  Created by Harjit Singh on 11/11/19.
//  Copyright © 2019 gmx. All rights reserved.
//

import UIKit

class SearchDisplayController: UIControl {
     
    init(x:CGFloat,y:CGFloat,height:CGFloat,width:CGFloat) {
        super.init(frame: CGRect(x: x, y: y, width: width, height: height))
        initialSetup()
    }
   
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func initialSetup(){
        appearance()
        let imageView = UIImageView(frame: CGRect(x: 15, y: self.frame.height/2 - 12, width: 24, height: 24))
        imageView.image = UIImage(named: "searchAbLight")
        imageView.tintColor = UIColor.init(red: 114/255, green: 114/255, blue: 114/255, alpha: 1)
        self.addSubview(imageView)
        let searchTextlabel = UILabel(frame: CGRect(x: self.frame.width/2 - (self.frame.width - 100)/2, y: 0, width: self.frame.width - 70, height: self.frame.height))
        searchTextlabel.font = UIFont(name: "Helvetica", size: 17)
        searchTextlabel.text = "Search, Assistance & Voice"
        searchTextlabel.textColor = UIColor.init(red: 114/255, green: 114/255, blue: 114/255, alpha: 1)
        self.addSubview(searchTextlabel)
    }
    
    private func appearance(){
        self.backgroundColor = .white
        self.layer.cornerRadius = 2.0
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2.0
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        self.layer.shadowOpacity = 0.6
    }

}
